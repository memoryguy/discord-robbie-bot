# Robbie Bot
## My first attempt at a Discord bot

## Introduction
The intention of this project is to create a Discord bot. The original feature
is a currency converter. After that, additional features are added as we think
of them.

## Requirements
- Ruby 2.4
- Discordrb module
 - https://github.com/meew0/discordrb
- Money
 - https://github.com/RubyMoney/money
- GoogleCurrency
 - https://github.com/RubyMoney/google_currency
