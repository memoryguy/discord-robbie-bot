#!/bin/bash

if [ "$DISCORD_APP_TOKEN" = "none" -o "$DISCORD_CLIENT_ID" = "none" ]; then
  echo "==================================================================="
  echo "===      ERROR - CONFIGURATION NOT COMPLETE                     ==="
  echo "==================================================================="
  echo
  echo "ERROR:  You must specify both DISCORD_APP_TOKEN and DISCORD_CLIENT_ID "
  echo "ERROR:  variables when starting the Docker container:"
  echo "ERROR:    -e DISCORD_APP_TOKEN=\"your-token\""
  echo "ERROR:    -e DISCORD_CLIENT_ID=\"your-id\""
  exit 1
fi

echo "app_token: \"$DISCORD_APP_TOKEN\"" >  /bot/config/auth.yml
echo "client_id: $DISCORD_CLIENT_ID"     >> /bot/config/auth.yml

rm -f tmp/quit-bot.dat
while true; do
  echo "Starting bot"
  bundle exec ruby robbie.rb

  # check for the exit flag; the bot was terminated with the DIE command
  # otherwise wait a little bit and then restart
  [ -f tmp/quit-bot.dat ] && break

  sleep 5
done
