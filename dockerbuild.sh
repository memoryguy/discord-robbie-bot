#!/bin/bash

echo "Prepare application files..."
echo "git_hash: \"$(git rev-parse HEAD)\"" > config/build.yml
rm -f tmp/app_data.tar.gz
tar cfz tmp/app_data.tar.gz config/ Gemfile* run_bot.sh *rb

echo "Cleaning up old containers"
docker stop robbie
docker rm   robbie
docker rmi  robbie

echo "Build image"
docker build --tag robbie -f Dockerfile .

echo "Start image"
docker run --name robbie -d \
       -e DISCORD_APP_TOKEN=$DISCORD_APP_TOKEN \
       -e DISCORD_CLIENT_ID=$DISCORD_CLIENT_ID robbie

echo "Clean up"
rm -f tmp/app_data.tar.gz
