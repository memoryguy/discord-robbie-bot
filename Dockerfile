FROM ruby:2.4

RUN mkdir -p /bot/log && \
    gem install bundler

ADD /tmp/app_data.tar.gz /bot

ENV DISCORD_APP_TOKEN none
ENV DISCORD_CLIENT_ID none

WORKDIR /bot
RUN bundle install

CMD /bot/run_bot.sh
