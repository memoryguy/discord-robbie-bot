require 'logger'
require 'discordrb'
require 'yaml'
require 'money'
require 'money/bank/google_currency'

require_relative 'lib/multi_io'
require_relative 'lib/my_string'


log_file = File.open("log/robbie.log", "a")
logger   = Logger.new MultiIO.new(STDOUT, log_file)

logger.info "Loading configuration"
unless File.exist?("config/auth.yml")
  logger.error "Unable to find config/auth.yml!"
  logger.error "Did you configure it?"
  exit 1
end

config = YAML.load_file("config/auth.yml")

if File.exist?("config/build.yml")
  build  = YAML.load_file("config/build.yml")
  logger.info "Running git hash: #{build["git_hash"]}" unless build["git_hash"].nil?
  logger.info "Running git tag:  #{build["git_tag"]}"  unless build["git_tag"].nil?
end

# Prepare the conversion machine...
# set the seconds after than the current rates are automatically expired
# by default, they never expire
I18n.enforce_available_locales = false
Money::Bank::GoogleCurrency.ttl_in_seconds = 3600
Money.default_bank = Money::Bank::GoogleCurrency.new
currencies = YAML.load_file("config/currencies.yml")
logger.info "Loaded #{currencies["currencies"].size} currencies"
quitting_time = false

cbot = Discordrb::Commands::CommandBot.new token: config["app_token"], client_id: config["client_id"], prefix: "~"
rbot = Discordrb::Bot.new token: config["app_token"], client_id: config["client_id"]

cbot.ready do |event|
  logger.info "Going online"
  cbot.online
end

rbot.ready do |event|
  @babble = Array.new
  File.open('technobabble.txt').each { |line| @babble << line.chomp }
  logger.info "Loading technobabble: #{@babble.size}"
end

# Register the bot command to flip a coin; randomly returns HEADS or TAILS
cbot.command :flip,
            help_available: true, description: "Flip a coin",
            min_args: 0, max_args: 0 do |event|
  outcomes = [ "HEADS", "TAILS" ]
  result   = outcomes[rand(0..1)]

  logger.info "#{event.author.username} flipped a coin: #{result}"

  "@#{event.author.username} flipped a coin and it came up #{result}"
end

# Register the bot command to convert currency
cbot.command :convert,
            help_available: true, description: "Convert an amount of money into other currencies",
            usage: "Convert 'amount' 'currency'",
            min_args: 2, max_args: 2 do |event, amount, currency|
  @message  = ""

  if amount.numeric?
    amount   = amount.to_f
    currency = currency.upcase
    result   = Hash.new
    retries  = 5

    begin
      # Money assumes that the amount is in cents. Multiply by 100 to turn it
      # into dollars. BUT, this doesn't work for currencies such as JPY that
      # don't have fractions (cents)
      source = Money.from_amount(amount, currency)
      logger.info "Converting #{source} #{source.currency}"
      logger.debug "Will skip source currency, #{currency}"

      # convert the value to all the currencieses
      currencies["currencies"].each do |curr|
        unless curr == currency
          result[curr] = Money.new(amount*100, currency).exchange_to curr
          logger.info "  -> #{result[curr]} #{curr}"
        end

        # convert the hash ( currency -> amount ) into a string for returning
        # to the user
        @message = "#{sprintf "%0.2f", amount} #{currency} is:\n#{result.map { |k, v| "  #{v} #{k}" }.join("\n")}"
      end
    rescue Money::Bank::UnknownRate => e
      logger.error "UnknownRate"
      @message = "@#{event.author.username} I don't know how to convert from that currency"
    rescue Money::Currency::UnknownCurrency => e
      logger.error "UnknownCurrency"
      @message = "@#{event.author.username} I don't recognize that currency"
    rescue Money::Bank::GoogleCurrencyCaptchaError => e
      logger.error "GoogleCurrencyCaptchaError"
      @message = "@#{event.author.username} Google doesn't think I'm a person :cry:"
    rescue Money::Bank::GoogleCurrencyFetchError => e
      logger.error "GoogleCurrencyFetchError"
      if retries > 0
        sleep 5
        retries -= 1
        retry
      else
        @message = "@#{event.author.username} Conversion isn't available right now. Try again in a moment"
      end
    end
  else
    @message = "Can only convert numbers, @#{event.author.username}"
    logger.info "#{event.author.username} asked for conversion of non-number '#{amount}' #{currency}"
  end

  @message
end

cbot.command :log,
            help_available: true, description: "Get the bot's logfile",
            usage: "log",
            min_args: 0, max_args: 0 do |event|
  logger.info "Sending log to user #{event.author.username}"

  event.author.send_file "log/robbie.log", "Bot log"
end

# Register the command to shut down the bot; this will also set the trigger to
# stop the Docker script from restarting
cbot.command :die,
            help_available: true, description: "Stop the bot process",
            usage: "die",
            min_args: 0, max_args: 0 do |event|
  logger.info "Shutting down due to !die command from #{event.author.username}"
  cbot.send_message(event.channel, "So long and thanks for all the fish")
  File.open("tmp/quit-bot.dat", "w") {}

  quitting_time = true
end

# Register the command to restart the bot
cbot.command :restart,
            help_available: true, description: "Restarts the bot process",
            usage: "restart",
            min_args: 0, max_args: 0 do |event|
  logger.info "Restarting due to !restart command from #{event.author.username}"
  cbot.send_message(event.channel, "I'll be back")

  quitting_time = true
end

rbot.message() do |event|
  triggers = [ "broke", "b0rk", "wreck" ]
  triggers.each do |trigger|
    if event.message.content.downcase.include? trigger
      rand_debug = @babble.sample
      event.respond "Have you tried #{rand_debug}?"
      logger.info "#{event.message.content} => #{rand_debug}"
      break
    else
      nil
    end
  end
end

logger.info "Connecting to Discord"
cbot.run :async
rbot.run :async

until quitting_time
  sleep 1
end

cbot.invisible
cbot.stop

